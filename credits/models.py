from django.db import models
from django.utils import timezone


# Абстрактные классы

class DateLoggedBase(models.Model):
    """
    Базовый класс для таблиц, имеющих дату и время создания / изменения
    """
    created_at = models.DateTimeField('дата создания', editable=False)
    updated_at = models.DateTimeField('дата изменения', editable=False)

    def save(self, *args, **kwargs):
        """
        Дейсвтия при сохранении объекта
        created_at - один раз при создании
        updated_at - при каждом обновлении
        """
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(DateLoggedBase, self).save(*args, **kwargs)

    class Meta:
        abstract = True


# Справочники

class RequestStatus(models.Model):
    name = models.CharField(max_length=10)  # Максимальная длина строки из статусов в ТЗ

    @classmethod
    def by_name(cls, name):
        return cls.objects.get(name=name)

    def __str__(self):
        return self.name


class OfferType(models.Model):
    name = models.CharField(max_length=15)  # Максимальная длина строки из статусов в ТЗ

    def __str__(self):
        return self.name


# Кредитные отношения и их участники

class Organization(models.Model):
    """
    Партнёр или кредитная организация.
    Операторы партнёра заполняют заявки клиентов.
    Операторы кредитной организации рассматривают заявки.
    """
    ORGANIZATION_TYPE_CHOICES = [
        ('Partner', 'Партнёр'),
        ('Creditor', 'Кредитная организация'),
    ]
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=8, choices=ORGANIZATION_TYPE_CHOICES, default='Partner')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Организацию'
        verbose_name_plural = 'Организации'


class CreditOffer(DateLoggedBase):
    """
    Предложение
    - ↑ дата и время создания (автоматически)
    - ↑ дата и время изменения (автоматически)
    - дата и время начала ротации
    - дата и время окончания ротации
    - название предложения
    - тип предложения (потреб, ипотека, автокредит)
    - минимальный скоринговый балл
    - максимальный скоринговый балл
    - кредитная организация - foreign key
    """
    start_rotation = models.DateTimeField('начало ротации')
    end_rotation = models.DateTimeField('окончание ротации')
    name = models.CharField('название предложения', max_length=50)
    type = models.ForeignKey(OfferType, verbose_name='тип предложения', on_delete=models.CASCADE, related_name='offers')
    min_rank = models.IntegerField('минимальный скоринговый балл')
    max_rank = models.IntegerField('максимальный скоринговый балл')
    creditor = models.ForeignKey(Organization, verbose_name='организация', on_delete=models.CASCADE,
                                 related_name='offers')

    @classmethod
    def actual(cls):
        tzn = timezone.now()
        return cls.objects.filter(start_rotation__lte=tzn, end_rotation__gt=tzn)

    def __str__(self):
        return '%s [%s]' % (self.name, self.type)

    class Meta:
        verbose_name = 'Предложение'
        verbose_name_plural = 'Предложения'


class Client(DateLoggedBase):
    """
    Анкета клиента
    - ↑ дата и время создания (автоматически)
    - ↑ дата и время изменения (автоматически)
    - Фамилия
    - Имя
    - Отчество
    - дата рождения
    - номер телефона
    - номер паспорта
    - скоринговый балл
    - партнер - foreign key
    """
    last_name = models.CharField('фамилия', max_length=50)
    first_name = models.CharField('имя', max_length=50)
    patronymic = models.CharField('отчество', max_length=50)
    dob = models.DateField('дата рождения')
    phone_number = models.CharField('номер телефона', max_length=15)
    passport_number = models.CharField('номер паспорта', max_length=10)
    rank = models.FloatField('скоринговый балл')
    partner = models.ForeignKey(Organization, verbose_name='организация', on_delete=models.CASCADE,
                                related_name='clients')

    def __str__(self):
        return '%s %s %s' % (self.last_name, self.first_name[0], self.patronymic[0])

    class Meta:
        verbose_name = 'Клиента'
        verbose_name_plural = 'Клиенты'


class CreditRequest(DateLoggedBase):
    """
    Заявка в кредитную организацию
    - ↑ дата и время создания
    - дата и время отправки
    - анкета клиента foreign key
    - предложение foreign key
    - статус (новая, отправлена, получена, одобрено, отказано, выдано)
    """
    sent_at = models.DateTimeField(editable=False, null=True)
    client = models.ForeignKey(Client, verbose_name='клиент', on_delete=models.CASCADE, related_name='requests')
    offer = models.ForeignKey(CreditOffer, verbose_name='предложение', on_delete=models.CASCADE,
                              related_name='requests')
    status = models.ForeignKey(RequestStatus, verbose_name='статус', on_delete=models.CASCADE, related_name='requests')

    def update_sent(self):
        self.sent_at = timezone.now()

    class Meta:
        verbose_name = 'Заявку'
        verbose_name_plural = 'Заявки'

    def __str__(self):
        return '%s - %s - %s' % (self.client, self.offer, self.status)