from django.contrib import admin
from custom_auth.models import CustomUser as User


class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'organization', 'last_login']
    readonly_fields = ['last_login', 'date_joined']
    fields = ['username', 'groups', 'organization', 'date_joined']

    def save_model(self, request, obj: User, form, change):
        if not obj.pk:
            obj.is_staff = True
            obj.is_active = True
        return super(UserAdmin, self).save_model(request, obj, form, change)


admin.site.register(User, UserAdmin)
