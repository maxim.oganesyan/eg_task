from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib import messages
from .models import CustomUser as User


def index(request):
    return redirect('/admin/')


def login_page(request):
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            if username:
                db_user = User.objects.get(username=username)
                if db_user and db_user.is_superuser:
                    user = authenticate(request, username=username, password=password)
                    if user:
                        login(request, user)
                        messages.add_message(request, messages.SUCCESS, 'Вход выполнен.')
                        return redirect('/admin')
        except:
            pass
        demo_user = request.POST['demo_user']
        if demo_user:
            user = User.objects.get(username=demo_user)
            if user:
                # user.is_active = True
                # user.is_staff = True
                user.save()
                login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                messages.add_message(request, messages.SUCCESS, 'Вход выполнен.')
                return redirect('/admin')
        messages.add_message(request, messages.ERROR, 'Пользователь не найден.')
    users = User.objects.filter(is_superuser=False).all()
    return render(request, 'auth/login.html', dict(users=users))
