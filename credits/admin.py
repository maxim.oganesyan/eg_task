from django.contrib import admin
from django.utils import timezone
from credits.models import Organization, CreditOffer, Client, CreditRequest, RequestStatus
from custom_auth import models as auth_model
from typing import Optional


# Фильтры
class ActualRotationCreditOfferFilter(admin.SimpleListFilter):
    title = 'Ротация'
    parameter_name = 'rotation'

    def lookups(self, request, model_admin):
        return (
            ('actual', 'актуальные'),
            ('future', 'будущие'),
            ('past', 'прошедшие')
        )

    def queryset(self, request, queryset):
        tzn = timezone.now()
        if self.value() == 'actual':
            return queryset.filter(start_rotation__lte=tzn, end_rotation__gt=tzn)
        if self.value() == 'future':
            return queryset.filter(start_rotation__gte=tzn)
        if self.value() == 'past':
            return queryset.filter(end_rotation__lt=tzn)


class CreditorOwnOfferFilter(admin.SimpleListFilter):
    """
    Для кредитных организаций показывает только лично созданные предложения
    """
    title = 'Организация'
    parameter_name = 'organization'

    def lookups(self, request, model_admin):
        current_user: auth_model.CustomUser = request.user
        return ('whatever', current_user.organization.name),

    def value(self):
        return 'whatever'

    def queryset(self, request, queryset):
        current_user: auth_model.CustomUser = request.user
        if (not current_user.is_superuser) and current_user.organization.type == 'Creditor':
            return queryset.filter(creditor__pk=current_user.organization.pk)


class PartnerOwnClientFilter(admin.SimpleListFilter):
    """
    Для партнёров показывает только лично созданных клиентов
    """
    title = 'Организация'
    parameter_name = 'organization'

    def lookups(self, request, model_admin):
        current_user: auth_model.CustomUser = request.user
        return ('whatever', current_user.organization.name),

    def value(self):
        return 'whatever'

    def queryset(self, request, queryset):
        current_user: auth_model.CustomUser = request.user
        if (not current_user.is_superuser) and current_user.organization.type == 'Partner':
            return queryset.filter(partner__pk=current_user.organization.pk)


class OrganizationCreditRequestFilter(admin.SimpleListFilter):
    """
    Для кредитных организаций показывает только запросы, направленные к ним
    Для партнёров показывает только лично созданные запросы
    """
    title = 'Организация'
    parameter_name = 'organization'

    def lookups(self, request, model_admin):
        current_user: auth_model.CustomUser = request.user
        return ('whatever', current_user.organization.name),

    def value(self):
        return 'whatever'

    def queryset(self, request, queryset):
        current_user: auth_model.CustomUser = request.user
        if (not current_user.is_superuser):
            if current_user.organization.type == 'Partner':
                return queryset.filter(client__partner__pk=current_user.organization.pk)
            else:  # -> current_user.organization.type == 'Creditor'
                return queryset.filter(offer__creditor__pk=current_user.organization.pk)\
                    .exclude(status__pk=RequestStatus.by_name('новая').pk)


# Администрирование
class OrganizationAdmin(admin.ModelAdmin):
    list_display = ['name', 'type']


class CreditOfferAdmin(admin.ModelAdmin):
    list_display = ['name', 'creditor', 'type', 'rank_range', 'is_actual']
    radio_fields = {'type': admin.VERTICAL}

    def is_actual(self, obj: CreditOffer) -> bool:
        tzn = timezone.now()
        return obj.start_rotation < tzn < obj.end_rotation

    def rank_range(self, obj: CreditOffer) -> str:
        return '%s - %s' % (obj.min_rank, obj.max_rank)

    is_actual.boolean = True
    is_actual.short_description = 'актуально'
    rank_range.short_description = 'скоринговый балл'

    def changelist_view(self, request, extra_context=None):
        current_user = request.user
        if (not current_user.is_superuser) and current_user.organization.type == 'Creditor':
            self.list_filter = [ActualRotationCreditOfferFilter, CreditorOwnOfferFilter]
            self.list_display = ['name', 'type', 'rank_range', 'is_actual']
        else:
            self.list_filter = [ActualRotationCreditOfferFilter]
            self.list_display = ['name', 'creditor', 'type', 'rank_range', 'is_actual']
        return super(CreditOfferAdmin, self).changelist_view(request, extra_context)

    def get_form(self, request, obj: Optional[CreditOffer] = None, change=False, **kwargs):
        current_user = request.user
        if (not current_user.is_superuser):
            if current_user.organization.type == 'Creditor':
                if obj and obj.creditor.pk != current_user.organization.pk:
                    self.fields = ['pk']
                    self.readonly_fields = ['pk']
                else:
                    self.fields = ['start_rotation', 'end_rotation', 'name', 'type', 'min_rank', 'max_rank',
                                   'created_at', 'updated_at']
                    self.readonly_fields = ['created_at', 'updated_at']
            else:
                self.fields = ['start_rotation', 'end_rotation', 'name', 'type', 'min_rank', 'max_rank', 'creditor',
                               'created_at', 'updated_at']
                self.readonly_fields = ['created_at', 'updated_at']
        return super(CreditOfferAdmin, self).get_form(request, obj, change, **kwargs)

    def save_model(self, request, obj: CreditOffer, form, change):
        current_user = request.user
        if (not current_user.is_superuser):
            if current_user.organization.type == 'Creditor':
                obj.creditor = current_user.organization
                return super(CreditOfferAdmin, self).save_model(request, obj, form, change)
        else:
            return super(CreditOfferAdmin, self).save_model(request, obj, form, change)


class ClientAdmin(admin.ModelAdmin):
    ordering = ['last_name']
    readonly_fields = ['created_at', 'updated_at']
    search_fields = ['last_name', 'first_name', 'phone_number']

    def changelist_view(self, request, extra_context=None):
        current_user = request.user
        if (not current_user.is_superuser) and current_user.organization.type == 'Partner':
            self.list_filter = [PartnerOwnClientFilter]
            self.list_display = ['__str__', 'last_name', 'first_name', 'patronymic', 'rank', 'phone_number',
                                 'updated_at']
        else:
            self.list_display = ['__str__', 'last_name', 'first_name', 'patronymic', 'rank', 'phone_number',
                                 'partner', 'updated_at']
        return super(ClientAdmin, self).changelist_view(request, extra_context)

    def get_form(self, request, obj: Optional[Client] = None, change=False, **kwargs):
        current_user = request.user
        if (not current_user.is_superuser):
            if current_user.organization.type == 'Partner':
                if obj and obj.partner.pk != current_user.organization.pk:
                    self.fields = ['pk']
                    self.readonly_fields = ['pk']
                else:
                    self.fields = ['last_name', 'first_name', 'patronymic', 'dob', 'phone_number', 'passport_number',
                                   'rank']
                    self.readonly_fields = ['created_at', 'updated_at']
            else:
                self.fields = ['last_name', 'first_name', 'patronymic', 'dob', 'phone_number', 'passport_number',
                               'rank', 'partner']
                self.readonly_fields = ['created_at', 'updated_at']
        return super(ClientAdmin, self).get_form(request, obj, change, **kwargs)

    def save_model(self, request, obj: Client, form, change):
        current_user = request.user
        if (not current_user.is_superuser):
            if current_user.organization.type == 'Partner':
                obj.partner = current_user.organization
                return super(ClientAdmin, self).save_model(request, obj, form, change)
        else:
            return super(ClientAdmin, self).save_model(request, obj, form, change)


class CreditRequestAdmin(admin.ModelAdmin):
    list_display = ['client', 'offer', 'status', 'sent_at', 'updated_at']
    readonly_fields = ['updated_at', 'sent_at']

    def changelist_view(self, request, extra_context=None):
        current_user = request.user
        if (not current_user.is_superuser):
            self.list_filter = [OrganizationCreditRequestFilter]
        return super(CreditRequestAdmin, self).changelist_view(request, extra_context)

    def get_form(self, request, obj: Optional[CreditRequest] = None, change=False, **kwargs):
        current_user = request.user
        if (not current_user.is_superuser):
            self.list_display = ['client', 'offer', 'status', 'sent_at', 'updated_at']
            if current_user.organization.type == 'Creditor':
                self.readonly_fields = ['client', 'offer', 'updated_at', 'sent_at']
            else:  # -> current_user.organization.type == 'Partner'
                ro = ['updated_at', 'sent_at']
                if obj and obj.status != RequestStatus.by_name('новая'):
                    ro.append('status')
                self.readonly_fields = ro
        else:
            self.list_display = ['client', 'offer', 'status', 'sent_at', 'created_at', 'updated_at']
            self.readonly_fields = ['updated_at', 'sent_at']
        return super(CreditRequestAdmin, self).get_form(request, obj, change, **kwargs)

    def save_model(self, request, obj: CreditRequest, form, change):
        current_user = request.user
        if (not current_user.is_superuser) and (not obj.pk):
            obj.status = RequestStatus.by_name('новая')
        if obj.status == RequestStatus.by_name('отправлена') and not obj.sent_at:
            obj.update_sent()
        return super(CreditRequestAdmin, self).save_model(request, obj, form, change)


admin.site.register(Organization, OrganizationAdmin)
admin.site.register(CreditOffer, CreditOfferAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(CreditRequest, CreditRequestAdmin)
