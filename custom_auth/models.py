from django.db import models
from credits.models import Organization
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import PermissionsMixin
from .managers import CustomUserManager


# Пользователи, роли, права

class CustomUser(AbstractUser):
    organization = models.ForeignKey(Organization, verbose_name='организация', on_delete=models.CASCADE,
                                     related_name='users', null=True)
    objects = CustomUserManager()

    @classmethod
    def by_username(cls, username):
        return cls.objects.filter(username=username).first()

    def has_perm(self, perm, obj=None):
        if self.is_superuser:
            return super().has_perm(self, perm)
        return perm in self.get_group_permissions()

    def has_perms(self, perm_list, obj=None):
        if self.is_superuser:
            return super().has_perms(self, perm_list)
        return all([self.has_perm(perm) for perm in self.get_group_permissions()])

    class Meta:
        verbose_name = 'Пользователя'
        verbose_name_plural = 'Пользователи'
