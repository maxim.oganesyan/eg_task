Проект подготовлен в качестве [тестового задания](docs/task/index.md) от потенциального работодателя.  

# Подготовка к установке
Команды ниже предполагают установку в Unix-подобную операционную систему. Для иных операционных систем рекомендуется
воспользоваться инструкцией в интернете.  
Перед установкой необходимо иметь установленные в системе `git` и `python` версии `3.x`, а также `virtualenv` и `pip`.  
## Процесс развёртывания
Клонировать проект
```shell script
$ git clone https://gitlab.com/maxim.oganesyan/eg_task.git
```
Внутри клонированного проекта необходимо создать и активировать виртуальную среду
```shell script
$ cd eg_task
$ virtualenv venv
$ source venv/bin/activate
```
Можно приступать к установке
# Установка
Внутри проекта выполнить следующие команды:
```shell script
(venv)$ pip install -r requirements.txt
(venv)$ python manage.py makemigrations
(venv)$ python manage.py migrate
(venv)$ python manage.py createsuperuser
```
После последней команды пройти анкетирование.  
При **чистой установке** необходимо выполнить команду, чтобы наполнить базу справочниками, 
группами и правами:
```shell script
(venv)$ python manage.py loaddata initial_data.yaml
(venv)$ python manage.py loaddata auth.yaml
```
Также при чистой установке необходимо [завести организации и пользователей](docs/instructions/clean_steps.md).

Если импортируется дамп (**не при чистой установке**), необходимо выполнить следующие команды:
```shell script
(venv)$ python manage.py loaddata auth.yaml
(venv)$ python manage.py loaddata custom_auth.yaml
(venv)$ python manage.py loaddata credits.yaml
```
 # Запуск
```shell script
(venv)$ python manage.py runserver
```
Страница будет доступна по адресу http://127.0.0.1:8000/

# Интерфейс
![login image](login.png)  
Чтобы войти под суперпользователем, необходимо заполнить Имя пользователя и Password.
Для входа под именами иных пользователей, достаточно выбрать любого из них в Demo User.

Фронт не написан, DRF отсутствует. Весь функционал реализован через админку.

[Инструкция по работе](docs/instructions/index.md)